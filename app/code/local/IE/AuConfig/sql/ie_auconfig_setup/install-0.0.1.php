<?php
/**
* Magento IE AuConfig Extension
*
* NOTICE OF LICENSE
*
* This source file is subject to the Magento Enterprise Edition License
*
* DISCLAIMER
*
* This custom extension is owned by IE Media and it licensed under IE.com.au
* Please do not modify this file cause you will lose the modified when upgrading it
*
* @category    Module
* @package     IE_AuConfig
* @copyright   Copyright (c) 2013 IE Media http://ie.com.au/
* @author      Dimas Putra - dputra@ie.com.au
*/

$installer = $this;

$installer->startSetup();

$config = new Mage_Core_Model_Config();

$config_path_data = array(

	//general tabs
        'general/country/default'  => 'AU',
        'general/locale/state_required'  => 'AU',
        'general/locale/code' => 'en_AU',
        'general/locale/timezone' => 'Australia/Sydney',

    //currency setup
        'currency/options/base' => 'AUD',
        'currency/options/default' => 'AUD',
        'currency/options/allow' => 'AUD',

    //catalog tabs
        'catalog/frontend/flat_catalog_product' => 1,
        'catalog/seo/category_canonical_tag' => 1,
        'catalog/seo/product_canonical_tag' => 1,

    //inventory tabs
        'cataloginventory/options/show_out_of_stock' => 1,
        'cataloginventory/item_options/manage_stock' => 0,

    //tax tabs
        'tax/classes/shipping_tax_class' => 4;
        'tax/defaults/country' => 'AU',
        'tax/defaults/region' => '0',
        'tax/calculation/algorithm' => 'TOTAL_BASE_CALCULATION',
        'tax/calculation/apply_after_discount' => 0,
        'tax/calculation/discount_tax' => 1,
        'tax/calculation/price_includes_tax' => 1,
        'tax/calculation/shipping_includes_tax' => 1,
        'tax/display/type' => 2,
        'tax/display/shipping' => 2,
        'tax/cart_display/price' => 2,
        'tax/cart_display/subtotal' => 2,
        'tax/cart_display/shipping' => 2,
        'tax/cart_display/discount' => 2,
        'tax/cart_display/gift_wrapping' => 2,
        'tax/cart_display/printed_card' => 2,
        'tax/sales_display/price' => 2,
        'tax/sales_display/subtotal' => 2,
        'tax/sales_display/shipping' => 2,
        'tax/sales_display/discount' => 2,
        'tax/sales_display/gift_wrapping' => 2,
        'tax/sales_display/printed_card' => 2,

    //shipping setting tabs
        'shipping/origin/country_id' => 'AU',
        'shipping/origin/postcode' => '3000',
        'shipping/origin/region_id' => '487'
);

// loop to save config using @Mage_Core_Model_Config
foreach ($config_path_data as $path => $value) {
    $config->saveConfig($path, $value, 'default', 0);
}

$installer->endSetup();