<?php
/**
* Magento IE AuConfig Extension
*
* NOTICE OF LICENSE
*
* This source file is subject to the Magento Enterprise Edition License
*
* DISCLAIMER
*
* This custom extension is owned by IE Media and it licensed under IE.com.au
* Please do not modify this file cause you will lose the modified when upgrading it
*
* @category    Module
* @package     IE_AuConfig
* @copyright   Copyright (c) 2013 IE Media http://ie.com.au/
* @author      Dimas Putra - dputra@ie.com.au
*/

class IE_AuConfig_Model_Resource_Setup extends Mage_Eav_Model_Entity_Setup 
{
    //return all from the parent
}
