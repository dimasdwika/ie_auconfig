<?php
/**
* Magento IE AuConfig Extension
*
* NOTICE OF LICENSE
*
* This source file is subject to the Magento Enterprise Edition License
*
* DISCLAIMER
*
* This custom extension is owned by IE Media and it licensed under IE.com.au
* Please do not modify this file cause you will lose the modified when upgrading it
*
* @category    Module
* @package     IE_AuConfig
* @copyright   Copyright (c) 2013 IE Media http://ie.com.au/
* @author      Dimas Putra - dputra@ie.com.au
*/

$installer = $this;
/**
 * install Australian GST calculation rule
 */
$data = array(
    'tax_calculation_rule_id'   => 2,
    'code'                      => 'GST',
    'priority'                  => 0,
    'position'                  => 0
);
$installer->getConnection()->insertForce($installer->getTable('tax/tax_calculation_rule'), $data);

/**
 * install Australian GST calculation rates
 */
$data = array(
    array(
        'tax_calculation_rate_id'   => 3,
        'tax_country_id'            => 'AU',
        'tax_region_id'             => 487,
        'tax_postcode'              => '*',
        'code'                      => 'GST',
        'rate'                      => '10.0000'
    )
);
foreach ($data as $row) {
    $installer->getConnection()->insertForce($installer->getTable('tax/tax_calculation_rate'), $row);
}

/**
 * install Australian GST calculation
 */
$data = array(
    array(
        'tax_calculation_rate_id'   => 3,
        'tax_calculation_rule_id'   => 2,
        'customer_tax_class_id'     => 3,
        'product_tax_class_id'      => 2
    )
);
$installer->getConnection()->insertMultiple($installer->getTable('tax/tax_calculation'), $data);

$installer->endSetup();